#include <cstdio>
#include <cstdlib>
#include <opencv2/core/core.hpp>
#include <opencv2/core/types_c.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <iostream>
#include <istream>
#include <vector>
#include <ctime>
#include <random>
#include <chrono>
#include <math.h>
#include <inttypes.h>
#include <algorithm>
#include <functional>
#define PI 3.141559265
#define ZMAX 800

using namespace cv;
using namespace std;

typedef struct {
    float ts; //seconds
    float x; //cms
    float y; //cms
    float theta; //radians
}odometryData;

typedef struct {
    float ts; //seconds
    float x; //cms
    float y; //cms
    float theta; //radians
    float xl; //cms
    float yl; //cms
    float thetal; //radians
    float range[45];
}laserData;


typedef struct {
    float x;
    float y;
    float theta;
    float z[45];
    Point2d zPoint[45];
    float weights;
}particles;

float RandomFloat(float min, float max) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = max - min;
    float r = random * diff;
    return min + r;
}


void createLaserOffset(vector<particles> &laserParticleVec, vector<particles> ParticleVec) {
    float l = 2.5;
    for (int i = 0; i < ParticleVec.size(); i++) {
        laserParticleVec[i].x = (ParticleVec[i].x) + l*cos(ParticleVec[i].theta);
        laserParticleVec[i].y = (ParticleVec[i].y) + l*sin(ParticleVec[i].theta);
        laserParticleVec[i].theta = ParticleVec[i].theta;
    }
}

/* --------------------- create random particle ----------------- */
void create_particles(vector<particles> &ParticleVec, Mat binaryMap){
        int offset = 2;
    float min_xy = 0 + offset; float max_xy = 800 - offset;
    srand(10000);
    for (int i = 0; i < ParticleVec.size(); i++) {
        ParticleVec[i].x = RandomFloat(min_xy, max_xy);
        ParticleVec[i].y = RandomFloat(min_xy, max_xy);
        int x_cood = ParticleVec[i].x;
        int y_cood = ParticleVec[i].y;
        if (binaryMap.at<uchar>(y_cood, x_cood) == 0 || binaryMap.at<uchar>(y_cood + offset, x_cood) == 0 || binaryMap.at<uchar>(y_cood - offset, x_cood) == 0 || binaryMap.at<uchar>(y_cood, x_cood + offset) == 0 || binaryMap.at<uchar>(y_cood, x_cood - offset) == 0) {
            i--;
            continue;
        }
        ParticleVec[i].theta = RandomFloat(-1*PI, PI);
        ParticleVec[i].weights = 0.f;
    }
 }

/* ------------------ Ray Tracing for Particles ------------------ */

void ray_tracing(vector<particles> &ParticleVec, int best_particle, Mat binaryMap, VideoWriter &output) {
    int res = 1;
    vector<particles> laserParticleVec(ParticleVec.size());
    createLaserOffset(laserParticleVec, ParticleVec);
//    ofstream file;
//    file.open("particles.txt", ios::app);
    Point best_laser [45];
    best_laser[0] = Point((int)laserParticleVec[best_particle].x, (int)laserParticleVec[best_particle].y);
    for (int i = 0; i < ParticleVec.size(); i++) {
        Point2f pt1(laserParticleVec[i].x, laserParticleVec[i].y); // 1 to 8000 range
        float theta = laserParticleVec[i].theta;
        for (int j = 1; j <= 45; j++) {
            float angle = (j*4-90)+((theta*180)/PI);
            angle = angle*PI/180;
            for (int k = 1; k <= 200; k++) {
                Point2d pt2((int)(pt1.x+(res*k*cos(angle))), (int)(pt1.y+(res*k*sin(angle))));
                if (pt2.x >= binaryMap.cols){
                    pt2.x = binaryMap.cols-1;
                    // ParticleVec[i].zPoint[j].x = pt2.x;
                }
                if ( pt2.x <= 0 ){
                    pt2.x = 0;
                    // ParticleVec[i].zPoint[j].x = pt2.x;
                }
                if (pt2.y >= binaryMap.rows) {
                    pt2.y = binaryMap.rows-1;
                    // ParticleVec[i].zPoint[j].y = pt2.y;
                }
                if (pt2.y <= 0){
                    pt2.y = 0;
                    // ParticleVec[i].zPoint[j].y = pt2.y;
                }
                if (pt2.x < binaryMap.cols && pt2.x >= 0 && pt2.y < binaryMap.rows && pt2.y >= 0) {
                    if (binaryMap.at<uchar>((int)pt2.y, (int)pt2.x) == 0) {
                        ParticleVec[i].z[j-1] = sqrt(((pt2.x-pt1.x)*(pt2.x-pt1.x)) + ((pt2.y-pt1.y)*(pt2.y-pt1.y)));
                        ParticleVec[i].zPoint[j-1].x = pt2.x;
                        ParticleVec[i].zPoint[j-1].y = pt2.y;
                        if (i == best_particle){
                            best_laser[j] = pt2; 
                        }
                        break;
                    }
                    else if(k == 200) {
                            ParticleVec[i].z[j-1] = sqrt(((pt2.x-pt1.x)*(pt2.x-pt1.x)) + ((pt2.y-pt1.y)*(pt2.y-pt1.y)));
                        ParticleVec[i].zPoint[j-1].x = pt2.x;
                        ParticleVec[i].zPoint[j-1].y = pt2.y;
                        if (i == best_particle){
                            best_laser[j] = pt2; 
                        }
                    }
                }
            }
        }
    }
     // cout << "ray tracing: 1" << endl;
    /* ------------- Visualizing Ray Tracing Results ------------------ */
    Mat colorMap(binaryMap.rows, binaryMap.cols, CV_8UC3);
    for (int r = 0; r < binaryMap.rows; r++) {
        for (int c = 0; c < binaryMap.cols; c++) {
            if (binaryMap.at<uchar>(r,c) == 0) {
                colorMap.at<Vec3b>(r,c).val[0] = 0;
                colorMap.at<Vec3b>(r,c).val[1] = 0;
                colorMap.at<Vec3b>(r,c).val[2] = 0;
            }
            else {
                colorMap.at<Vec3b>(r,c).val[0] = 255;
                colorMap.at<Vec3b>(r,c).val[1] = 255;
                colorMap.at<Vec3b>(r,c).val[2] = 255;
            }
        }
    }
     // cout << "ray tracing: 2" << endl;
    // int countRays = 0;
    for (int i = 0; i < ParticleVec.size(); i++) {
//        file << ParticleVec[i].x << ", " << ParticleVec[i].y << endl;
        colorMap.at<Vec3b>((int)(ParticleVec[i].y), (int)(ParticleVec[i].x)).val[0] = 0;
        colorMap.at<Vec3b>((int)(ParticleVec[i].y), (int)(ParticleVec[i].x)).val[1] = 0;
        colorMap.at<Vec3b>((int)(ParticleVec[i].y), (int)(ParticleVec[i].x)).val[2] = 255;
        for(int d = 0; d < 45; d++) {
            // countRays++;
//            file << ParticleVec[i].zPoint[d].x << ", " << ParticleVec[i].zPoint[d].y << endl;
            colorMap.at<Vec3b>(ParticleVec[i].zPoint[d].y, ParticleVec[i].zPoint[d].x).val[0] = 255;
            colorMap.at<Vec3b>(ParticleVec[i].zPoint[d].y, ParticleVec[i].zPoint[d].x).val[1] = 255;
            colorMap.at<Vec3b>(ParticleVec[i].zPoint[d].y, ParticleVec[i].zPoint[d].x).val[2] = 0;
        }
    }
    /* --------- Draw the best particle -------------- */
    Point center((int)ParticleVec[best_particle].x, (int)ParticleVec[best_particle].y);
    int radius = 5;
    Point circ_point;
    circ_point.x = (int)(center.x + radius*cos(ParticleVec[best_particle].theta));
    circ_point.y = (int)(center.y + radius*sin(ParticleVec[best_particle].theta));

    circle(colorMap, center, radius, Scalar(255,0,0));
    arrowedLine(colorMap, center, circ_point, Scalar(255,0,0));
    fillConvexPoly(colorMap, best_laser, 46, Scalar(0,255,0));


//    file << "________________________________________" << endl;
//    file << "________________________________________" << endl;
     // cout << "ray tracing: 3" << endl;
    // cout << "rays: " << countRays << endl;
//    file.close();
    // flip(colorMap,colorMap, -1);
    // transpose(colorMap, colorMap);
    // flip(colorMap,colorMap, 1);
    if (output.isOpened() == true){
        output.write(colorMap);
    }
    return;
}

/* --------------------- measurement model ---------------------- */

void measurement_model( laserData xt, vector <particles> &xt2, int &best_particle, Mat binaryMap) {
    // xt: laser measurements
    // xt2: particle measurements
    float lambdaShort = 0.2119;
    float zk;
    float sum = 0, q = 0, sum_weights = 0, best_weight = 0;
    for (int i = 0; i < xt2.size(); i++) {
        q = 0;
        for (int j = 0; j < 45; j++) {
            zk = xt.range[j];
            // float sum[ZMAX]; //probabilities of each possibility depending on where the angle lies
            // float pnorm[ZMAX];
            // float globalSum = 0;
            // for (int k = 0; k < ZMAX; k++) {
            sum = 0;
            if (zk >= 5.5 && zk <= xt2[i].z[j]) {
                // etaShort = 0.1/(1-exp(-lambdaShort*xt2[i].z[j]));
                sum += 0.2*0.1289 *exp(-1*lambdaShort*zk); // 0.2 is to reduce the weight of the expnential term.
            }
            if (zk <= ZMAX) {
                sum += 0.05837*exp(-pow(((zk-xt2[i].z[j])/4.814),2)); // standard deviation 5.814 
                sum += 0.0008696;
            }
            if (zk ==ZMAX) {
                sum += 0.005429;
            }
            sum *= 0.25;
            // globalSum += sum[k];
            // }
            // for (int k = 0; k < ZMAX; k++) {
            //     pnorm[k] = sum[k]/globalSum;
            // }
            // pZk = pnorm[(int)(xt.range[j])];
            q += log(sum);
        }
        xt2[i].weights = exp(q/45);
        sum_weights += xt2[i].weights;
    }
    // float check_weights = 0;
    printf("Sum weights = %f \n",sum_weights);
    // if (sum_weights < 1){
    //  low_vareiance_re_sampling(ParticleVec);
    // }
    // if (log(sum_weights) < -14){
    //      create_particles(xt2, binaryMap);
    // }

    // else {
            for(int k = 0; k < xt2.size(); k++){
                  xt2[k].weights = xt2[k].weights/sum_weights;
                  if (xt2[k].weights > best_weight){
                    best_weight = xt2[k].weights;
                    best_particle = k;
                  }
            // xt2[k].weights = pow(xt2[k].weights,0.2);
            // check_weights += xt2[k].weights;
                // }
    }
    // printf("Sum of normalized weights = %f \n",check_weights);
    // transform(xt2.weights.begin(), xt2.weights.end(), xt2.weights.begin(), bind1st(multiplies<float>(),(float)1/sum_weights));
}


/* --------------------- motion model ---------------------- */
float randn (float mu, float sigma)
{
    float U1, U2, W, mult;
    static float X1, X2;
    static int call = 0;
    if (call == 1)
    {
        call = !call;
        return (mu + sigma * (float) X2);
    }
    do
    {
        U1 = -1 + ((float) rand () / RAND_MAX) * 2;
        U2 = -1 + ((float) rand () / RAND_MAX) * 2;
        W = pow (U1, 2) + pow (U2, 2);
    }
    while (W >= 1 || W == 0);
    mult = sqrt ((-2 * log (W)) / W);
    X1 = U1 * mult;
    X2 = U2 * mult;
    call = !call;
    return (mu + sigma * (float) X1);
}



void motion_model(odometryData xBar1, odometryData xBar2, vector <particles> &xt, Mat binaryMap){
    //xt - particles from measurement model
        int max_count = 10;
        int count = 0, num_particles_skipped = 0;
    float alpha1 = 0.01;
    float alpha2 = 0.05;
    float dTrans = sqrt(((xBar1.x-xBar2.x)*(xBar1.x-xBar2.x)) + ((xBar1.y-xBar2.y)*(xBar1.y-xBar2.y)));
    float dRot = xBar2.theta - xBar1.theta;
    float dRot1 = atan2((xBar2.y-xBar1.y), (xBar2.x-xBar1.x)) - xBar1.theta;
    float dRot2 = xBar2.theta - xBar1.theta - dRot1;
    if (dRot == 0.0 && dTrans == 0.0){
        return;
    }
    vector<particles> xt_temp_vec(1), temp_laserParticleVec(1);

    for (int i = 0; i < xt.size(); i++) {
    //     float tempdRot1 = randn(dRot1, sqrt(alpha1*pow(dRot1,2)+alpha2*pow(dTrans,2)));
    //     float tempdTrans = randn(dTrans, sqrt(alpha1*pow(dRot1,2)+alpha1*pow(dRot2,2) + alpha2*pow(dTrans,2)));
                // float tempdRot2 = randn(dRot2, sqrt(alpha2*pow(dTrans,2)+alpha1*pow(dRot2,2)));

            float tempdRot1 = randn(dRot1, alpha1*dRot1+alpha2*dTrans);
        float tempdTrans = randn(dTrans, alpha1*dRot1 + alpha1*dRot2 + alpha2*dTrans);
                float tempdRot2 = randn(dRot2, alpha2*dTrans + alpha1*dRot2);

                xt_temp_vec[0].x = xt[i].x+tempdTrans*cos(xt[i].theta +tempdRot1);
        xt_temp_vec[0].y = xt[i].y+tempdTrans*sin(xt[i].theta +tempdRot1);
        xt_temp_vec[0].theta = xt[i].theta + tempdRot1 + tempdRot2;
        // cout << "Random x-position before check: " << xt[i].x+tempdTrans*cos(xt[i].theta +tempdRot1) << ", " << xt_temp_vec[0].x << endl;

                if (xt_temp_vec[0].x >= 800)
            xt_temp_vec[0].x = 799;
        if (xt_temp_vec[0].y >= 800)
            xt_temp_vec[0].y = 799;
        if (xt_temp_vec[0].x < 0)
            xt_temp_vec[0].x = 0;
        if (xt_temp_vec[0].y < 0)
            xt_temp_vec[0].y = 0;
//        file << xt[i].x << ", " << xt[i].y << ", i: " << i << "map: "<< (int)binaryMap.at<uchar>((int)(xt[i].y/10), (int)(xt[i].x/10)) <<  endl;
        
        // xt_temp_vec.push_back(xt_temp);
        // cout << "Size of temp_xt: " << temp_xt.size() << endl;
        createLaserOffset(temp_laserParticleVec, xt_temp_vec);
        // cout << "reached these particles: " << i << endl;
        if (count <= max_count){
            if ((int)(temp_laserParticleVec[0].y) >= 800 || (int)(temp_laserParticleVec[0].x) >= 800 || (int)(temp_laserParticleVec[0].y) < 0 || (int)(temp_laserParticleVec[0].x) < 0) {
                i--;
            // cout << "Outside the Map dimensions: " << i << endl;
                count++;
                continue;
            }
            if (binaryMap.at<uchar>((int)(temp_laserParticleVec[0].y), (int)(temp_laserParticleVec[0].x))==0 || binaryMap.at<uchar>((int)(xt_temp_vec[0].y), (int)(xt_temp_vec[0].x))==0) {
            // cout << "Outside the white zone: (y,x)" << (int)temp_laserParticleVec[0].y << "," << (int)temp_laserParticleVec[0].x << endl;
            // cout << "Particle position - post update: (y,x,theta)" << xt_temp_vec[0].y << "," << xt_temp_vec[0].x << "," << xt_temp_vec[0].theta << endl;
            // cout << "Particle position - pre update: (y,x,theta)" << xt[i].y << "," << xt[i].x << "," << xt[i].theta << endl;
            // cout << "Delta Translation: " << dTrans << endl;
            // cout << "Random Translation, Rotation: " << tempdTrans << "," << tempdRot1 << "," << tempdRot2 << endl;
            // cout << "Translation, Rotation: " << dTrans << "," << dRot1 << "," << dRot2 << "," << atan2((xBar2.y-xBar1.y), (xBar2.x-xBar1.x))<<  endl;
            // cout << "Random x-position: " << xt_temp_vec[0].x << ", " << xt[i].x <<  endl;
            // cout << "Random y-position: " << xt_temp_vec[0].y << ", " << xt[i].y <<  endl;
                i--;
                count++;
                continue;
            }
            xt[i].x = xt_temp_vec[0].x;
            xt[i].y = xt_temp_vec[0].y;
            xt[i].theta = xt_temp_vec[0].theta;
      }
      else{
        count = 0;
        num_particles_skipped++;
      }
    }
    cout << "num_particles_skipped = " << num_particles_skipped << endl;
}

/* -------------------- re-sampling --------------------------- */
/* -------------------- variance --------------------------- */
float variance (vector<particles> &ParticleVec){
        float M = ParticleVec.size();
        float sum = 0;
        for(int i = 0; i < M; i++){
                sum += (ParticleVec[i].weights - 1/M)*(ParticleVec[i].weights - 1/M);
        }
        return sum/M;
}

void low_vareiance_re_sampling (vector<particles> &ParticleVec) {
    float var_weights = log(variance(ParticleVec));
    cout << "Variance of the particles: " << var_weights << endl;
    if (var_weights > -20.5){
            vector<particles> temp_ParticleVec = ParticleVec;
            float r, c, u, M;
            int i = 0;
            M = ParticleVec.size();
            r = RandomFloat(0,1/M);
            cout << "RandomFloat: " << r << endl;
            c = temp_ParticleVec[i].weights;
            for(int j = 0; j < M; j++){
                u = r + (float)((float)(j)*1/M);
                // u = pow(u,0.2);
                while(u > c){
                    i += 1;
                    if (i < M){
                            c += temp_ParticleVec[i].weights;
                    }
                    if (i >= M){
                        break;
                    }
                }
                // printf("Current weight = %f", c);
                if (i < M){
                        ParticleVec[j].x = temp_ParticleVec[i].x;
                        ParticleVec[j].y = temp_ParticleVec[i].y;
                        ParticleVec[j].theta = temp_ParticleVec[i].theta;
                        ParticleVec[j].weights = temp_ParticleVec[i].weights;
                }
        }
    }
}


int main(int argc, char **argv) {
    
    if (argc < 2) {
        cout << "Usage: ./main <robot_data filename> <map filename> "  << endl;
    }
    
    string dataFile = argv[1];
    string mapFile = argv[2];
    
    ifstream data(dataFile);
    ifstream map(mapFile);
    if (data.good() == 0) {
        cout << "odometry log file doesn't exist" << endl;
    }
    if (map.good()==0) {
        cout << "map file doesn't exist" << endl;
    }
    vector <odometryData> OdometryDataVec;
    vector <laserData> LaserDataVec;
    vector <particles> ParticleVec(10000);
    vector <int> labels; //-1 for laser, 1 odometry data
    

    
    Mat mapData(800, 800, CV_32F);
    Mat binaryMap(800, 800, CV_8UC1);
    Mat binaryMapColor(800, 800, CV_8UC3);
/* ------------------- Parsing Wean.dat file --------------- */
    while (!map.eof()) {
        string line;
        getline(map, line);
        if (line[0]=='g')
            break;
    }
    while (!map.eof()) {
        for (int i = 0; i < 800; i++) {
            for (int j = 0; j < 800; j++) {
                map >> mapData.at<float>(i,j);
                if (mapData.at<float>(i,j) >= 0.995) {
                    binaryMap.at<uchar>(i,j) = 255;
                    binaryMapColor.at<Vec3b>(i,j).val[0] = 255;
                    binaryMapColor.at<Vec3b>(i,j).val[1] = 255;
                    binaryMapColor.at<Vec3b>(i,j).val[2] = 255;
                }
                else {
                    binaryMap.at<uchar>(i,j) = 0;
                    binaryMapColor.at<Vec3b>(i,j).val[0] = 0;
                    binaryMapColor.at<Vec3b>(i,j).val[1] = 0;
                    binaryMapColor.at<Vec3b>(i,j).val[2] = 0;
                }

            }
        }
    }
    
/* -------------------- Initializing particles ----------------- */
    flip(binaryMap,binaryMap, -1);
    transpose(binaryMap, binaryMap);
    flip(binaryMap,binaryMap, 0);
    flip(binaryMapColor,binaryMapColor, -1);
    transpose(binaryMapColor, binaryMapColor);
    flip(binaryMapColor,binaryMapColor, 0);
    create_particles(ParticleVec, binaryMap);

/* ------------------ Visualizing particles ------------------- */
    for (int i = 0; i < ParticleVec.size(); i++) {
        int x_cood = ParticleVec[i].x;
        int y_cood = ParticleVec[i].y;
        binaryMapColor.at<Vec3b>(y_cood,x_cood).val[0] = 0;
        binaryMapColor.at<Vec3b>(y_cood,x_cood).val[1] = 0;
        binaryMapColor.at<Vec3b>(y_cood,x_cood).val[2] = 255;
    }
    imwrite("WorkingMap.jpg",binaryMapColor);
//    ofstream filemap;
//    filemap.open("map.txt");
//    filemap << mapData;
//    imshow("binarymapColor", binaryMapColor);
//    waitKey();
    cout << "DONE" << endl;
    
/* -------------- Parsing data file (log files) -------------------- */
    while (!data.eof()) {
        string temp;
        data >> temp;
        if (temp[0] =='L') {
            laserData tempD;
            data >> tempD.x;
            tempD.x /= 10;
            data >> tempD.y;
            tempD.y /= 10;
            data >> tempD.theta;
            
            data >> tempD.xl;
            tempD.xl /= 10;
            data >> tempD.yl;
            tempD.yl /= 10;
            data >> tempD.thetal;
            
            for (int i=1, j=0; i <=180; i++) {
                float temp_theta;
                    if (i%4 == 0){
                    data >> tempD.range[j];
                    tempD.range[j] /= 10;
                    j++;
                    }
                else
                    data >> temp_theta;
            }
            data >> tempD.ts;
            LaserDataVec.push_back(tempD);
            labels.push_back(-1);
        }
        
        if (temp[0] == 'O') {
            odometryData tempO;
            data >> tempO.x;
            tempO.x /= 10;
            data >> tempO.y;
            tempO.y /= 10;
            data >> tempO.theta;
            data >> tempO.ts;
            OdometryDataVec.push_back(tempO);
            labels.push_back(1);
        }
    }
    
/* ---------------------- Measurement and Motion model ------------------- */
    // int laserIter = 0;
    // running measurement model to get x_0 (x_(t-1)) needed for the first iteration
    // of the motion model
    
    // xt1: laser measurements
    // xt2: particle measurements
    /* ---------------- Video writer ------------------ */

    VideoWriter video_output;
    dataFile.replace(dataFile.find(".log"), sizeof(".avi")-1, ".avi");
    video_output.open (dataFile, CV_FOURCC('M','J','P','G'), 10, cv::Size (800,800), true);


    int laserCount = 0;
    int odometryCount = 1;
    int best_particle = 0;
    for (int i = 0; i < labels.size(); i++) {
            // random_shuffle(ParticleVec.begin(), ParticleVec.end());
            // cout << "Particles Size: " << ParticleVec.size();
        cout << "i: " << i << ", label : "<< labels[i] << endl;
        if ( labels[i] == -1) {
                // if (laserCount%5 != 0){
                //      laserCount++;
                //      // ray_tracing(ParticleVec, binaryMap, video_output);
                //      continue;
                // }
            ray_tracing(ParticleVec, best_particle, binaryMap, video_output);
            cout << "ray_tracing done" << endl;
            time_t before = time(0);
            cout << "measurement model: laserCount: " << laserCount << endl;
            measurement_model(LaserDataVec[laserCount], ParticleVec, best_particle, binaryMap);
            time_t after = time(0);
            cout << "time taken for measurement: " << after-before  << " seconds" << endl;
            laserCount += 1;
            if(laserCount%2 == 0 && laserCount >=2){
                    low_vareiance_re_sampling(ParticleVec);
            }
            continue;
        }
        
        if (labels[i] == 1) {
//            ofstream fb;
//            fb.open("pB.txt");
//            for (int p = 0; p < ParticleVec.size(); p++) {
//                fb << ParticleVec[p].x << ", " << ParticleVec[p].y << endl;
//            }
//            fb.close();
            motion_model(OdometryDataVec[odometryCount-1], OdometryDataVec[odometryCount], ParticleVec, binaryMap);
            // ofstream fA;
//            fA.open("pA.txt");
//            for (int p = 0; p < ParticleVec.size(); p++) {
//                fA << ParticleVec[p].x << ", " << ParticleVec[p].y << endl;
//            }
//            fA.close();
            cout << "motion model: OdometeryCount: "<< odometryCount << endl;
            odometryCount += 1;
//            i = i+23;
            // if(i%8 == 0 && laserCount >= 1){
            //      low_vareiance_re_sampling(ParticleVec);
            // }
            continue;
            
        }
        // if (i%100 == 0){
        //     low_vareiance_re_sampling(ParticleVec);
        // }
    }
    video_output.release();
  return 0;

}

