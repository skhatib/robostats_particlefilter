A global localization filter for a lost robot.

Lost robot is operating in Wean Hall with nothing but odometry and a laser rangefinder.

run instructions:
*** Requires Open CV 2
1. cmake ~/Downloads/<folder_name>
2. make
3. ./main <robot_data filename> <map filename>
sample run:
./main <folder_name>/log/robotdata1.log <folder_name>/map/wean.dat 
./main <folder_name>/log/robotdata2.log <folder_name>/map/wean.dat 

*** Video files will be generated in log/ folder


